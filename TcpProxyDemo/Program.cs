﻿using System;
using System.Net;
using System.Text;
using System.IO;

using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace TcpProxyDemo
{
    class Program
    {
        const int BufferSize = 1024;

        static string HostnameToIp(string hostname)
        {
            string remoteIp = "";
            Console.WriteLine($"[DNS] Resolving {hostname} to IP...");
            IPHostEntry hostEntry = Dns.GetHostEntry(hostname);
            if (hostEntry.AddressList.Length > 0)
            {
                remoteIp = hostEntry.AddressList[0].ToString();
                Console.WriteLine($"[DNS] IP: {remoteIp}");
            }
            return remoteIp;
        }

        static private void WriteTcpMessage(NetworkStream stream, string responseStr)
        {
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseStr);
            stream.Write(buffer, 0, buffer.Length);
        }

        static private string ReadTcpMessage(NetworkStream stream)
        {

            var message = new byte[BufferSize];
            while (stream.DataAvailable)
            {
                try
                {
                    int clientBytes = stream.Read(message, 0, BufferSize);
                    Console.WriteLine($"[TCP] Read {clientBytes} bytes");
                    if (clientBytes == 0)
                    {
                        break;
                    }
                }
                catch
                {
                    break;
                }
            }
            return System.Text.Encoding.Default.GetString(message);
        }

        static private bool SslValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslpolicyerrors)
        {
            return true;
        }

        static void AcceptConnection(TcpClient client)
        {
            Console.WriteLine("[TCP] TCP proxy server accepted a new connection");
            string request = ReadTcpMessage(client.GetStream());
            Console.WriteLine(request);
            foreach (string line in request.Split('\n'))
            {
                if (line.StartsWith("CONNECT"))
                {
                    foreach (string cmd in line.Split(' '))
                    {
                        if (cmd.Contains(":443"))
                        {
                            var hostname = cmd.Substring(0, cmd.IndexOf(":443"));
                            Console.WriteLine($"[Debug] Server remote hostname: {hostname}");
                            WriteTcpMessage(client.GetStream(), "HTTP/1.1 200 Connection established\r\n\r\n");
                            new Task(() => Mitm(hostname, 443, client)).Start();
                        }
                    }
                }
            }
        }

        static string ReadSSLStream(Stream stream)
        {
            byte[] resultBuffer = new byte[BufferSize];
            string value = "";
            do
            {
                try
                {
                    int read = stream.Read(resultBuffer, 0, resultBuffer.Length);
                    value += Encoding.UTF8.GetString(resultBuffer, 0, read);

                    if (read < resultBuffer.Length)
                        break;
                }
                catch { break; }
            } while (true);
            Console.WriteLine($"[SSL] Read:{value.Length}");
            Console.WriteLine(value);
            return value;
        }

        static public void WriteSSLStream(Stream stream, byte[] data)
        {
            Console.WriteLine($"[SSL] Write:{data.Length}");
            stream.Write(data, 0, data.Length);
        }

        static void Mitm(string remoteHostname, int remotePort, TcpClient client)
        {
            Stream stream = client.GetStream();
            var clientStream = new SslStream(stream, false);
            Console.WriteLine("[SSL] Establishing client SSL stream over existing TCP stream");
            clientStream.AuthenticateAsServer(new X509Certificate("sslserver3.pfx", "123"), false, System.Security.Authentication.SslProtocols.Tls, false);
            Console.WriteLine("[SSL] CLient SSL stream established");
            
            Console.WriteLine("[TCP] Establishing SSL stream to remote server");
            var ip = HostnameToIp(remoteHostname);
            Console.WriteLine($"[TCP] Connecting to {ip}, port {remotePort}");
            var server = new TcpClient(ip, remotePort); // ip of web-server requested by client
            Console.WriteLine("[SSL] Tunneling to SSL");
            var serverSslStream = new SslStream(server.GetStream(), false, SslValidationCallback, null);
            Console.WriteLine($"[SSL] Connecting to {remoteHostname} using SSL");
            serverSslStream.AuthenticateAsClient(remoteHostname);
            Console.WriteLine("[SSL] Stream established, reading data");
            

            while (true)
            {
                Console.WriteLine("[SSL] Client read (request to server)");
                string clientRequest = ReadSSLStream(clientStream);

                Console.WriteLine("[SSL] Client write to server");
                WriteSSLStream(serverSslStream, Encoding.UTF8.GetBytes(clientRequest));

                Console.WriteLine("[SSL] Server read (response to client)");
                string data = ReadSSLStream(serverSslStream);

                Console.WriteLine("[SSL] Write response from server to client");
                WriteSSLStream(clientStream, Encoding.UTF8.GetBytes(data));

                Console.WriteLine("Hit Enter to close streams");
                var res = Console.ReadLine();
                if (string.IsNullOrEmpty(res))
                {
                    break;
                }
            }
            Console.WriteLine("[SSL] Closing SSL tunnels");
            serverSslStream.Close();
            clientStream.Close();
            client.Close();
            Console.WriteLine("[SSL] End");
        }

        static void Main()
        {
            TcpListener listener = new TcpListener(IPAddress.Any, 8777);
            Console.WriteLine($"TCP proxy server is listening on port {8777}");
            listener.Start();
            while (true)
            {
                try
                {
                    var client = listener.AcceptTcpClient();
                    AcceptConnection(client);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    break;
                }
            }
            listener.Stop();
        }
    }
}
